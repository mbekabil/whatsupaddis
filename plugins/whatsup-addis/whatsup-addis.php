<?php
/**
 * Created by PhpStorm.
 * User: mussebekabil
 * Date: 28/12/16
 * Time: 14:52
 */
/*
Plugin Name: Whats up Addis
Description:
Version: 1.0
Author: Automattic
Text Domain: whatsup addis
*/
define( 'WA_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once __DIR__ . '/shortcodes.php';

function custom_redirect () {
    global $post;
    if ( is_page() || is_object( $post ) ) {
        if ( $redirect = get_post_meta($post->ID, 'redirect', true ) ) {
            wp_redirect( $redirect );
            exit;
        }
        }
}
//Loade shortcodes
add_shortcodes();

add_action( 'get_header', 'custom_redirect' );
require_once( WA_PLUGIN_DIR . 'event.php' );

// init custom metaboxes
add_action( 'init', function() {
    if ( !class_exists( 'cmb_Meta_Box' ) ) {
        include WA_PLUGIN_DIR.'/../custom-metaboxes/init.php';
    }
}, 9999 );