<?php
/**
 * Created by PhpStorm.
 * User: mussebekabil
 * Date: 05/01/17
 * Time: 15:44
 */

require_once( __DIR__ . '/event.php');

function add_shortcodes() {
    add_shortcode('events', 'list_all_events');
}

function list_all_events() {
    $events = Event::get_posts();
    if( is_array($events) && !is_null($events) ) {
        echo '<div class="wa-events">';
        //print_r($events);
        foreach($events as $event ) {
            echo sprintf('<div class="wa-event col-sm-4"><div class="wa-event-thumbnail">%1$s</div><br />'.
                         '<div class="wa-event-details"><h3>%2$s</h3>'.
                         '<div class="wa-event-content">%3$s</div></div></div>',
                        get_the_post_thumbnail($event->ID, 'thumbnail'),
                        $event->post_title,
                        $event->post_content);
        }
        echo '</div>';

    }

}
