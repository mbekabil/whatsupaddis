<?php
/**
 * Created by PhpStorm.
 * User: mussebekabil
 * Date: 28/12/16
 * Time: 15:38
 */

class Event {
    static $instance = false;
    const CUSTOM_POST_TYPE = 'events';

    function __construct() {
        $me = $this;

        $custom_post_type = self::CUSTOM_POST_TYPE;

        add_action( 'init', function () use($custom_post_type) {
            register_post_type($custom_post_type,
                array(
                    'labels' => array(
                        'name' => __( 'Events', 'wp-admin' ),
                        'singular_name' => __( 'Event', 'wp-admin' )
                    ),
                    'public' => true,
                    'has_archive' => false,
                    'show_ui' => true,
                    'taxonomies' => array(
                        'category',
                    ),
                    'supports' => array(
                        'title',
                        'revisions',
                        'thumbnail',
                        'editor',
                        'excerpt',
                        'custom-fields',
                    ),
                    'rewrite' => array(
                        'slug' => 'events',
                        'with_front' => false,
                        'feeds' => false,
                        'pages' => false,
                    )
                )
            );

        });

        add_filter( 'cmb_meta_boxes', function($meta_boxes) use($me) {
            $prefix = 'wa_';
            $box = array(
                'id' => 'event_info',
                'title' => __('Event Details', 'wp-admin'),
                'pages' => array(Event::CUSTOM_POST_TYPE), // post type
                'context' => 'normal',
                'priority' => 'high',
                'show_names' => true, // Show field names on the left
                'fields' => array(
                    array(
                        'name' => __('Category', 'wp-admin'),
                        'id'   => $prefix . 'vehicle_type',
                        'type' => 'select',
                        'options' => array(
                            array('value' => '', 'name' => 'Choose'),
                            array('value' => 'art', 'name' => 'Art'),
                            array('value' => 'culture', 'name' => 'Culture'),
                            array('value' => 'music', 'name' => 'Music'),
                            array('value' => 'spiritual', 'name' => 'Spiritual'),
                        ),

                    ),
                    array(
                        'name' => __('Read more', 'wp-admin'),
                        'id' => $prefix . 'read_more',
                        'type' => 'text',
                    ),
                ),
            );

            $meta_boxes[] = $box;

            return $meta_boxes;
        });

        /**
         * Flush rewrite rules when saving vehicle information.
         * Otherwise these might be unavailable when changing slugs.
         */
        add_action( 'save_post', function($post_id) {
            global $wp_rewrite;
            if(get_post_type($post_id) == Event::CUSTOM_POST_TYPE) {
                $wp_rewrite->flush_rules(false);
            }
        });

        $post_type = self::CUSTOM_POST_TYPE;
    }

    /**
     * @return boolean
     */
    static function has_post($slug) {
        $post = self::get_post($slug);

        return $post !== false;
    }

    static function get_posts() {
        $args = array(
                    'post_type' => self::CUSTOM_POST_TYPE
                    );
        $posts = get_posts($args);

        return $posts;
    }

    static function get_post($slug) {
        static $cache = array();
        if(!isset($cache[$slug])) {
            $args = array(
                'name' => $slug,
                'numberposts' => 1,
                'offset'=> 0,
                'post_type' => self::CUSTOM_POST_TYPE
            );

            $posts = get_posts( $args );

            if(count($posts) > 0) {
                $ret = $posts[0];
            } else {
                $ret = false;
            }

            $cache[$slug] = $ret;
        } else {
            $ret = $cache[$slug];
        }

        return $ret;
    }

    static function get_post_id($slug) {
        $post = self::get_post($slug);
        if($post === false) {
            return false;
        }

        return $post->ID;
    }
    function get_post_for_mop_parsel($mop, $currency, $preselected_method){
        $post_array= array(
            'post_type' => self::CUSTOM_POST_TYPE,
            'meta_query' => array(
                array(
                    'key' => "cw_method_of_payment",
                    'value' => $mop,
                ),array(
                    'key' => "cw_currency",
                    'value' => $currency,
                ),array(
                    'key' => "cw_preselected_method",
                    'value' => $preselected_method,
                ),
            ),
        );
        $post_list = get_posts($post_array);
        if(empty($post_list)){
            return false;
        }
        else{
            return $post_list[0];
        }
    }
    function get_post_for_mop_standard($mop, $currency){
        $post_array= array(
            'post_type' => self::CUSTOM_POST_TYPE,
            'meta_query' => array(
                array(
                    'key' => "cw_method_of_payment",
                    'value' => $mop,
                ),array(
                    'key' => "cw_currency",
                    'value' => $currency,
                ),
            ),
        );
        $post_list = get_posts($post_array);
        if(empty($post_list)){
            return false;
        }
        else{
            return $post_list[0];
        }
    }

    /**
     * @return 	Event
     */
    static function &instance() {
        if(false === self::$instance)
            self::$instance = new self();

        return self::$instance;
    }
}

Event::instance();

class Event_Post {
    private $post_id = false;

    function __construct($post_id) {
        $this->post_id = $post_id;
    }

}

